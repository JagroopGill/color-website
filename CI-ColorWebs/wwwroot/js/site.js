// Please see documentation at https://docs.microsoft.com/aspnet/core/client-side/bundling-and-minification
// for details on configuring this project to bundle and minify static web assets.

// Write your JavaScript code.
const canvas = document.querySelector("canvas");
const context = canvas.getContext("2d");
const savedColor = [null,null,null,null,null,null];
let currColor = [null,null,null,null,null,null]
let lock1 = false, lock2 = false, lock3 = false, lock4 = false, lock5 = false, lock6 = false;
const cre = () => {
    var ranColor = [];
    const namesFirst = ["Beautiful ", "Splash of ", "Colorful ", "Dreary ", "Radiant " ,"Vibrant ", "Awe Struck ", "Taste of ", "True ", "Fresh", "Vivid", "Daring", "Sweet"];
    const namesLast = ["Morning", "Color", "Dreams", "Inspiration", "Wonder", "Midnight", "Hue" , "Paradise", "Passion"];
    const genNameFirst = namesFirst[Math.floor(Math.random() * 9)];
    const genNameLast = namesLast[Math.floor(Math.random() * 9)];
    if (lock1)
    {
        savedColor[0] = currColor[0]
    } else{
        savedColor[0] = null
    }
    if (lock2)
    {
        savedColor[1] = currColor[1]
    } else{
        savedColor[1] = null
    }
    if (lock3)
    {
        savedColor[2] = currColor[2]
    } else{
        savedColor[2] = null
    }
    if (lock4)
    {
        savedColor[3] = currColor[3]
    } else{
        savedColor[3] = null
    }
    if (lock5)
    {
        savedColor[4] = currColor[4]
    } else{
        savedColor[4] = null
    }
    if (lock6)
    {
        savedColor[5] = currColor[5]
    } else{
        savedColor[5] = null
    }
    for (var i = 0; i <= 5; i++){
        const randomColor = "000000".replace(/0/g,function(){return (~~(Math.random()*16)).toString(16);});
        ranColor.push(randomColor)
    }
    for (var i = 0; i <= 5; i++){
        if (savedColor[i] != null) {
            ranColor[i] = savedColor[i];
        }
    }
    palName.innerHTML = "\""+genNameFirst + genNameLast+"\"";
    document.getElementById("palName").style.fontSize = "xx-large";
    color.innerHTML = "#" + ranColor[0];
    context.fillStyle = '#' +ranColor[0];
    context.fillRect(0, 0, 166, 200);
    document.getElementById("color").style.color = "#" + ranColor[0];
    color2.innerHTML = "#" + ranColor[1];
    context.fillStyle = '#' +ranColor[1];
    context.fillRect(166, 0, 166, 200);
    document.getElementById("color2").style.color = "#" + ranColor[1];
    color3.innerHTML = "#" + ranColor[2];
    context.fillStyle = '#' +ranColor[2];
    context.fillRect(332, 0, 166, 200);
    document.getElementById("color3").style.color = "#" + ranColor[2];
    color4.innerHTML = "#" + ranColor[3];
    context.fillStyle = '#' +ranColor[3];
    context.fillRect(498, 0, 166, 200);
    document.getElementById("color4").style.color = "#" + ranColor[3];
    color5.innerHTML = "#" + ranColor[4];
    context.fillStyle = '#' +ranColor[4];
    context.fillRect(664, 0, 166, 200);
    document.getElementById("color5").style.color = "#" + ranColor[4];
    color6.innerHTML = "#" + ranColor[5];
    context.fillStyle = '#' +ranColor[5];
    context.fillRect(830, 0, 166, 200);
    document.getElementById("color6").style.color = "#" + ranColor[5];
    ailink = "?prompt=flower%20color(" + ranColor[0]+",%20" + ranColor[1]+ ",%20"+ ranColor[2]+ ",%20"+ ranColor[3]+ ",%20"+ ranColor[4]+ ",%20"+ ranColor[5]+ ")%20painting";
    var link = "<a href='https://www.craiyon.com/" + ailink + "' target='_blank'>here</a>";
    document.getElementById("linkai").innerHTML=link;
    currColor = ranColor
}

const lockedf1 = () => {
    lock1 = !lock1;
}
const lockedf2 = () => {
    lock2 = !lock2;
}
const lockedf3 = () => {
    lock3 = !lock3;
}
const lockedf4 = () => {
    lock4 = !lock4;
}
const lockedf5 = () => {
    lock5 = !lock5;
}
const lockedf6 = () => {
    lock6 = !lock6;
}

genNew.addEventListener("click", cre);
cre();
locked1.addEventListener("click",lockedf1);
locked2.addEventListener("click",lockedf2);
locked3.addEventListener("click",lockedf3);
locked4.addEventListener("click",lockedf4);
locked5.addEventListener("click",lockedf5);
locked6.addEventListener("click",lockedf6);


function lockToggle() {
    const btn = document.getElementById('locked1')
        btn.classList.toggle('lockClose')
    }

function lockToggle2() {
    const btn = document.getElementById('locked2')
    btn.classList.toggle('lockClose')
}

function lockToggle3() {
    const btn = document.getElementById('locked3')
    btn.classList.toggle('lockClose')
}

function lockToggle4() {
    const btn = document.getElementById('locked4')
    btn.classList.toggle('lockClose')
}

function lockToggle5() {
    const btn = document.getElementById('locked5')
    btn.classList.toggle('lockClose')
}

function lockToggle6() {
    const btn = document.getElementById('locked6')
    btn.classList.toggle('lockClose')
}



