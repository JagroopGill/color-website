
const canvas = document.querySelector("canvas");
const context = canvas.getContext("2d");

const bre = () => {
    const dict = [];
    dict[1] = "#6F3144 #A1552A #CB6042 #D48341 #FBAB34 #CF9B9B";
    dict[2] = "#CBBED8 #F8D6CB #CCC969 #E5CEB9 #9B8B81 #242424";
    dict[3] = "#f0213b #ba8f2a #984900 #b9a68a #7f093f #434a16";
    dict[4] = "#1e81b0 #eeeee4 #e28743 #eab676 #76b5c5 #21130d";
    dict[5] = "#FFFFC2 #FFBEE2 #E7BDF1 #DAFFC1 #FCFF78 #FDB0B0";
    dict[6] = "#18B8FE #1A6785 #3776BD #2D50B1 #3324E4 #159A76";
    dict[7] = "#FAB162 #FB0D12 #18AE26 #3E2816 #FE7707 #FEB163";
    dict[8] = "#0B2C02 #232922 #1C150D #413629 #773A03 #876239";
    dict[9] = "#C1A676 #9A9D99 #504745 #9F7054 #DEDBD3 #676A5E";
    dict[10] = "#0A4C60 #148284 #84C9AF #E3D196 #E88007 #BC5207";
    dict[11] = "#FFD6DA #FFC6D0 #E1F7E7 #D1EAF5 #C3D6F2 #768CCE";
    dict[12] = "#204362 #16879E #97E5DB #5FD1B8 #5C5C60 #CACACA";
    dict[13] = "#FFEADA #FFD5A4 #FFB5A4 #FDEAC3 #F4CBB1 #C8A18F";
    dict[14] = "#F0E6E4 #F7D8DF #F47D85 #DF4C5B #989E6A #5D6E4A";
    dict[15] = "#DBE5CE #6FB1B4 #3A727F #274B58 #F2D58F #F9ECD1";
    dict[16] = "#37D3EE #D5A6C7 #F99DB0 #B0D197 #F0E9EB #FAC5D4";
    dict[17] = "#F9D8C5 #FCBDAB #94D183 #6EBDA8 #269493 #20485A";
    dict[18] = "#37898C #9DD8D4 #C4E9E4 #F0D9C7 #FCD0D0 #F79CB1";
    dict[19] = "#235680 #B0D5D5 #E0F0ED #F9B0CC #BB648D #C999BB";
    dict[20] = "#7A5758 #DBBBBC #C897C4 #AD7CB3 #975794 #294867";
    dict[21] = "#C44953 #E86C74 #F48F82 #F7BBA0 #6D5F69 #E7E6E4";
    dict[22] = "#DCF4F5 #ACE0DD #FA6690 #FEC0CF #FEEBF2 #AE7F6C";
    dict[23] = "#EADFF2 #DCCBED #FEE5EB #FCB7D0 #F07BBB #9979C1";
    dict[24] = "#FDBF9C #F17173 #E23F6A #C63E81 #823E88 #AA66A5";
    dict[25] = "#276359 #8EBBA7 #B9CDCB #F2DDCC #FCAC89 #E38150";
    dict[26] = "#D4E3DE #A8BBB9 #A3B995 #F8DDB8 #FBBD96 #E88067";
    dict[27] = "#B3DFEC #B9C0EA #E8A5CC #F282A7 #F0725C #F6AC5A";
    dict[28] = "#8FDBD8 #B8DEAA #10B48E #F775A9 #FEAAC2 #F9EDD1";
    dict[29] = "#B8DEE8 #D4E5C5 #FDE3DC #FBB9B1 #2D8AA9 #F3EEE9";
    dict[30] = "#F4D7CF #CDD885 #E3EBB8 #EFE8DB #EDB18B #ECCD86";
    dict[31] = "#71BBEE #86DDF3 #FEB2DA #8474C7 #AE8FDC #D395E0";
    dict[32] = "#FADA88 #8BCDE5 #4893C0 #5155A0 #7256A6 #B39FD5";
    dict[33] = "#FFDFDF #FEB9B7 #2E6A67 #3C6654 #98B08B #DBDEBA";
    dict[34] = "#D0E6A6 #BFD37A #89B2AE #5B818E #234257 #293E3F";
    dict[35] = "#B8E4DC #9ED2BA #A0CA68 #6A9951 #4A696C #41545B";
    dict[36] = "#92CFD0 #E2D0DC #D8B2CC #AD7B9D #9EB88E #C5D6AC";
    dict[37] = "#FFB1B9 #FACBBA #F0CC96 #5B5759 #D5E8E2 #7ADDD1";
    dict[38] = "#2B78A2 #6EA8C9 #B6DEE7 #FCD9DD #F9E2CF #F2CDAF";
    dict[39] = "#FCEDBE #F6CF80 #EE816A #F7C9D3 #EE84A8 #D35A7F";
    dict[40] = "#5F98A9 #8AB6C6 #BDD7DF #E7BB8D #F2D6B8 #E5E5E5";
    dict[41] = "#E58E61 #F3BE95 #3D6E8D #CCDA9C #E8D5CF #F2EBE5";
    dict[42] = "#D1A4BD #B87A9E #F8C865 #FFA755 #D04A3E #C2845E";
    dict[43] = "#134080 #1D6590 #EF978F #FFC2AD #FFDFAC #1E2236";
    dict[44] = "#459C9C #2E8E9A #206175 #B5E3C7 #9CD5AD #EAC49E";
    dict[45] = "#FDACBB #FFABAA #FA8A89 #C2CB81 #5F936A #357D71";
    dict[46] = "#CD0056 #F15D8E #13626D #0C755F #A2C69B #E9D8CF";
    dict[47] = "#E1E4EA #D2C3D5 #84709B #464879 #5D7DB3 #B6CAD7";
    dict[48] = "#99D0D9 #5C9DBA #4D6782 #726286 #D7AC93 #926851";
    dict[49] = "#EFCFC5 #D77F57 #E2A388 #86C7CC #41A8A8 #DAE6E7";
    dict[50] = "#F0D0C7 #DEB49F #EBB1A3 #EEA8AB #F5B8BC #837881";
    dict[51] = "#FC9F66 #FAC357 #FAE39C #B8E0E3 #97C3D8 #84A9CD";
    dict[52] = "#1E7C88 #034B5D #A6522B #FC9D6F #E87A54 #DA5E3C";
    dict[53] = "#FA7991 #FBB0C1 #FBD6E5 #FEEFF4 #F2E6CE #E9D3A8";
    dict[54] = "#F47B8F #F3AABB #F5CDD3 #EDEDED #A1DCD8 #51AEAE";
    dict[55] = "#FAE9DD #FCE8ED #F8D9E1 #F9D1D1 #8E6E70 #514247";
    dict[56] = "#7FBAB7 #A1CBB7 #E8EFD5 #F7E5DF #E6C3BF #9C8384";
    dict[57] = "#638A66 #A5BDA2 #C0E1D9 #F0EBD6 #F1CBB5 #D2977C";
    dict[58] = "#A077FF #D6BBFF #FFCAF8 #FE86C1 #40CBEA #9CE8EE";
    dict[59] = "#FFAFCE #FF8AB8 #FD5E98 #37618B #1E3C69 #E43C6F";
    dict[60] = "#D4343F #F3505A #ECEBEA #085979 #008EA0 #64BABE";
    dict[61] = "#DAB3DA #766092 #A378B5 #55A6CB #537496 #ABD4E0";
    dict[62] = "#B33A89 #E46381 #FFC35E #FEDF47 #D3DF6C #EDE2E0";
    dict[63] = "#447B66 #96CCA8 #C0E1D1 #E0ECE6 #C6D589 #4C5B5D";
    dict[64] = "#D0A292 #C7BEAF #EAE3D1 #E2D1BF #BF967B #D08C7D";
    dict[65] = "#564335 #987861 #BB9887 #F7F3E7 #F3D7CC #E0B5A2";
    dict[66] = "#D7CFDB #C7BBD7 #828DBB #16285F #3F418D #5B64A4";
    dict[67] = "#EBEAE5 #E4DED0 #E2F2EF #C3DFE3 #A5CFCD #7FBBB2";
    dict[68] = "#DFE6EC #C5E1EC #C8D9EB #B6D1E2 #8FBEDC #338FC2";
    dict[69] = "#E8DCF4 #DBD1E9 #CDC6E7 #989BBE #AED7ED #3681AB";
    dict[70] = "#72D6EE #E7F7EC #D4E8E9 #A1CFFF #89BFFB #2DA0FA";
    dict[71] = "#6192A4 #8BC1D1 #B3D3DA #CEC7FD #A374CC #7E83C2";
    dict[72] = "#2F4F75 #99BDDF #D5E9F4 #F0E9F9 #CBBBD6 #9A9CC2";
    dict[73] = "#D8E2EB #C8C7D7 #EDECF2 #8897AA #F4EDE5 #D1E8EE";
    dict[74] = "#CACDA5 #99A285 #4E4F3D #636851 #A3A277 #A0B179";
    dict[75] = "#B6B9B9 #D0CEC8 #C7C5AF #A4A382 #7B7950 #50523D";
    
    let number = Math.floor(Math.random() * 74) + 1;
    const phrase = dict[number];
    const words = phrase.split(' ');
    const leColor = [];
    for (let i = 0; i <= 5; i++)
    {
        leColor.push(words[i]);
    }
    const namesFirst = ["Beautiful ", "Splash of ", "Colorful ", "Dreary ", "Radiant " ,"Vibrant ", "Awe Struck ", "Taste of ", "True ", "Fresh", "Vivid", "Daring", "Sweet"];
    const namesLast = ["Morning", "Color", "Dreams", "Inspiration", "Wonder", "Midnight", "Hue" , "Paradise", "Passion"];
    const genNameFirst = namesFirst[Math.floor(Math.random() * 9)];
    const genNameLast = namesLast[Math.floor(Math.random() * 9)];
    palNameTrue.innerHTML = "\""+genNameFirst + genNameLast+"\"";
    document.getElementById("palNameTrue").style.fontSize = "xx-large";
    
    palColor.innerHTML = leColor[0];
    context.fillStyle = leColor[0];
    context.fillRect(0, 0, 166, 200);
    document.getElementById("palColor").style.color = leColor[0];
    palColor2.innerHTML = leColor[1];
    context.fillStyle = leColor[1];
    context.fillRect(166, 0, 166, 200);
    document.getElementById("palColor2").style.color = leColor[1];
    palColor3.innerHTML = leColor[2];
    context.fillStyle = leColor[2];
    context.fillRect(332, 0, 166, 200);
    document.getElementById("palColor3").style.color = leColor[2];
    palColor4.innerHTML = leColor[3];
    context.fillStyle = leColor[3];
    context.fillRect(498, 0, 166, 200);
    document.getElementById("palColor4").style.color = leColor[3];
    palColor5.innerHTML = leColor[4];
    context.fillStyle = leColor[4];
    context.fillRect(664, 0, 166, 200);
    document.getElementById("palColor5").style.color = leColor[4];
    palColor6.innerHTML = leColor[5];
    context.fillStyle = leColor[5];
    context.fillRect(830, 0, 166, 200);
    document.getElementById("palColor6").style.color = leColor[5];
    Tailink = "?prompt=flower%20color(" + leColor[0].replace(/^#+/, '')+",%20" + leColor[1].replace(/^#+/, '')+ ",%20"+ leColor[2].replace(/^#+/, '')+ ",%20"+ leColor[3].replace(/^#+/, '')+ ",%20"+ leColor[4].replace(/^#+/, '')+ ",%20"+ leColor[5].replace(/^#+/, '')+ ")%20painting";
    var Tlink = "<a href='https://www.craiyon.com/" + Tailink + "' target='_blank'>here</a>";
    document.getElementById("Tlinkai").innerHTML=Tlink;
}

document.getElementById("gsetNew").addEventListener("click", bre);
bre();

document.addEventListener('keyup', event =>
{
    if (event.code === 'Space') 
    {
        event.preventDefault();
        bre();
    }
})
