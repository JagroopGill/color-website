using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace CI_ColorWebs.Pages;

public class TruePaletteModel : PageModel
{
    private readonly ILogger<TruePaletteModel> _logger;

    public TruePaletteModel(ILogger<TruePaletteModel> logger)
    {
        _logger = logger;
    }
    
    public void OnGet()
    {
    }
}